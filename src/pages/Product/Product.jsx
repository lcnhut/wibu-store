import React from "react";
import "./Product.scss";
import Carousel from "./Carousel";

export default function Product() {
  return (
    <div>
      <Carousel />
    </div>
  );
}
